/**
 * 変更前のものだよ
 *
 * @return {void}
 */
function hoge() {
  console.log('hoge');
}

/**
 * 変更1
 *
 * @return {void}
 */
function hoge2(arg) {
  console.log(arg);
}

/**
 * 変更2
 *
 * @return {void}
 */
function hoge3(arg) {
  console.log(arg);
}

/**
 * 変更3
 *
 * @return {void}
 */
function hoge4(arg) {
  console.log(arg);
}

